#!/bin/sh

COMMAND=$1

if [ "$COMMAND" == "" ]
then
    echo "Usage: source-import.sh <command>"
    exit 0
fi

FILES=$(ls *.tar.gz)
FILES+=($(ls *.jar))

$COMMAND new-sources "${FILES[@]}"
