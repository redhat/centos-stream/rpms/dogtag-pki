#!/bin/sh -e

download() {
    PKG_NAME=$1
    SUFFIX=$2

    ssh "${OPTIONS[@]}" $SOURCE dnf install -y $PKG_NAME
    VERSION=$(ssh "${OPTIONS[@]}" $SOURCE rpm -q $PKG_NAME | sed -n "s/^$PKG_NAME-\\([^-]*\\)-.*$/\\1/p")

    if [ "$VERSION" == "" ]
    then
        echo "$PKG_NAME not found"
        exit 1
    fi

    echo "Downloading $PKG_NAME-$VERSION"

    FILES=$(ssh "${OPTIONS[@]}" $SOURCE rpm -ql $PKG_NAME | sed -n -e "/^\/usr\/share\/java\/.*\.jar$/p")
    for FILE in $FILES
    do
        FILENAME=$(basename $FILE)
        NAME=$(echo $FILENAME | sed 's/\.jar$//')
        echo "Downloading $FILE to $NAME-$VERSION$SUFFIX.jar"
        scp "${OPTIONS[@]}" $SOURCE:$FILE $NAME-$VERSION$SUFFIX.jar
    done
}

SOURCE=$1

if [ "$SOURCE" == "" ]
then
    echo "Usage: source-download.sh <username>@<hostname>"
    exit 0
fi

OPTIONS=(-i /usr/share/qa-tools/1minutetip/1minutetip)

download jakarta-activation
download jakarta-annotations
download jaxb-api

download jackson-annotations
download jackson-core
download jackson-databind
download jackson-module-jaxb-annotations
download jackson-jaxrs-providers
download jackson-jaxrs-json-provider

download jboss-jaxrs-2.0-api .Final
download jboss-logging .Final

download pki-resteasy-core .Final
download pki-resteasy-client .Final
download pki-resteasy-jackson2-provider .Final
download pki-resteasy-servlet-initializer .Final
